import xlsxwriter
from sendEmail import sendEmail
import const
import openpyxl


def readAllRows():
    path = "Topic_Email.xlsx"
    wb_obj = openpyxl.load_workbook(path)
    sheet_obj = wb_obj.active
    max_col = sheet_obj.max_column
    max_row = sheet_obj.max_row

    sheet_data = []
    for i in range(1, max_row + 1):
        # Will print a particular row value
        row = []
        for j in range(1, max_col + 1):
            cell_obj = sheet_obj.cell(row=i, column=j)
            row.append(cell_obj.value)
        sheet_data.append(row)
    return sheet_data


def GetMailIdForTopic(topic):
    for row in readAllRows():
        if str(row[0]) == topic:
            return row[3]
    return None


class ExcelFunctions:

    def __init__(self):

        # self.wb = load_workbook(src)
        # self.ws = self.wb.get_sheet_by_name("Sheet1")
        # self.dest = "destination.xlsx"

        self.workbook = xlsxwriter.Workbook(const.EXCEL_FILEPATH)
        self.worksheet = self.workbook.add_worksheet("Kafka Topic Lag")
        column = 0

        for item in const.EXCEL_HEADER:
            self.worksheet.write(0, column, item)
            column += 1

    def AddRow(self, row_data, row_no):

        column = 0
        for item in row_data:
            self.worksheet.write(row_no, column, item)
            column += 1

    def SaveData(self, receiver_mail):

        self.workbook.close()
        sendEmail(receiver_mail)
