import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import const

password = input("Type your password and press enter:")


def sendEmail(receiver_email, *additional_info):
    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = const.SENDER_EMAIL
    message["To"] = receiver_email
    message["Subject"] = const.EMAIL_SUBJECT
    # message["Bcc"] = receiver_email  # Recommended for mass emails

    # Add body to email
    message.attach(MIMEText(const.EMAIL_BODY + const.EMAIL_SIGNATURE, "plain"))

    # filename = "document.pdf"  # In same directory as script

    # Open PDF file in binary mode
    with open(const.EXCEL_FILEPATH, "rb") as attachment:
        # Add file as application/octet-stream
        # Email client can usually download this automatically as attachment
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {const.EXCEL_FILEPATH}",
    )

    # Add attachment to message and convert message to string
    message.attach(part)
    text = message.as_string()

    # Log in to server using secure context and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(const.SENDER_EMAIL, password)
        try:
            server.sendmail(const.SENDER_EMAIL, receiver_email.split(','), text)
            print('Email sent')
        except smtplib.SMTPAuthenticationError:
            print("Wrong Credentials Detected")