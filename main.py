import smtplib
import sys
from time import sleep

from ExcelFunctions import ExcelFunctions, readAllRows
from KafkaConsumerFunctions import KafkaConsumerLag
import const


def CheckLagForTopicsPartition(kafka_consumer, topic, group, max_lag, receiver_mail):
    excel_file = ExcelFunctions()
    KafkaTopicsLag = kafka_consumer.CheckLagForTopic(topic)
    print(topic,KafkaTopicsLag)
    if sum(KafkaTopicsLag) > max_lag:
        excel_file.AddRow([topic, group, sum(KafkaTopicsLag)], 1)
        excel_file.SaveData(receiver_mail)


def main():
    kafka_consumer = KafkaConsumerLag(const.KAFKA_CONSUMER_GROUP, const.KAFKA_CONSUMER_BOOTSTRAP_SERVERS)
    while True:
        for row in readAllRows():
            CheckLagForTopicsPartition(kafka_consumer, row[0], row[1], row[2], row[3])
        sleep(5)
    # while True:
    #     CheckLagForTopicsPartition(kafka_consumer, const.KAFKA_CONSUMER_TOPIC1)
    #     CheckLagForTopicsPartition(kafka_consumer, const.KAFKA_CONSUMER_TOPIC2)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted By User")
        sys.exit(0)
    except smtplib.SMTPResponseException:
        print('Error in sending email')
    except smtplib.SMTPAuthenticationError:
        print('Wrong Credentials Detected')
