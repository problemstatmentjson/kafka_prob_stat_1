from kafka import KafkaConsumer, TopicPartition


class KafkaConsumerLag:

    def __init__(self, group, bootstrap_server):
        self.consumer = KafkaConsumer(
            bootstrap_servers=bootstrap_server,
            group_id=group,
            enable_auto_commit=False
        )

    def GetPartitionLag(self, topic, partition):
        tp = TopicPartition(topic, partition)
        self.consumer.assign([tp])
        committed = self.consumer.committed(tp)
        self.consumer.seek_to_end(tp)
        last_offset = self.consumer.position(tp)
        lag = last_offset - committed
        return lag

    def CheckLagForTopic(self, topic):
        partitions = self.consumer.partitions_for_topic(topic)
        topic_partition_lags = []
        for partition in partitions:
            topic_partition_lags.append(self.GetPartitionLag(topic, partition))

        return topic_partition_lags
